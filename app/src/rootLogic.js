
import fetchLogic   from './features/Quiz/fetchLogic';
import gameEndLogic from "./features/Quiz/gameEndLogic";

export default [
    fetchLogic,
    gameEndLogic
];