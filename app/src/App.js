import React, {Component} from 'react';
import {HomeScreen}         from "./features/Home/components/HomeScreen";
import QuizScreen         from "./features/Quiz/components/QuizScreen";
import ResultScreen       from "./features/Result/components/ResultScreen";
import history            from "./appHistory";
import {routes}           from "./routes";
import {Router as Router, Route} from 'react-router-dom'

class App extends Component {
    render() {
        return (
            <Router history = {history}>
                <div>
                    <Route path={'/'} component={HomeScreen}/>
                    <Route path={routes.home} component={HomeScreen}/>
                    <Route path={routes.quiz} component={QuizScreen}/>
                    <Route path={routes.result} component={ResultScreen}/>
                </div>
            </Router>
        );
    }
}

export default App;
