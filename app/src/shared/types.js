const types = {
    FETCH_QUIZLIST         : '[QUIZLIST FETCH]',
    QUIZLIST_FETCH_RESOLVED: '[QUIZLIST FETCH RESOLVED]',
    FETCH_QUIZLIST_REJECTED: '[QUIZLIST FETCH REJECTED]',
    UPDATE_QUIZ            : '[QUIZ UPDATE]',
    RESET_GAME             : '[GAME RESET]'
};

export default types;