import React, {Component} from 'react';
import './Header.css';


class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {text} = this.props;
        return (
            <header id="header">
                {text.split('\n').map((item, key) => {
                    return <div key={key}>{item}<br/></div>
                })}
            </header>
        );
    }
}


export default Header;