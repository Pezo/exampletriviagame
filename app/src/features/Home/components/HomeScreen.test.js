import React      from 'react';
import {shallow}  from 'enzyme';
import {HomeScreen} from './HomeScreen';

it('should have proper classes', () => {
    let _HomeScreen = shallow(<HomeScreen/>);
    expect(_HomeScreen).toMatchSnapshot();
});

