import React, {Component} from 'react';
import Header             from "../../../shared/components/Header";
import {Link}             from 'react-router-dom'
import {routes}           from "../../../routes";
import './HomeScreen.css';

export class HomeScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="home">
                <div className="welcome">
                    <Header text="Welcome to the Trivia Challange!"/>
                </div>
                <div className="goal">
                    You will be presented with 10 True or False questions.
                </div>
                <div className="question">
                    Can you score 100%?
                </div>
                <Link className="begin" to={routes.quiz}>
                    BEGIN
                </Link>
            </div>
        );
    }
}

