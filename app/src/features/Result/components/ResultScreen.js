import React, {Component}         from 'react';
import Header                     from "../../../shared/components/Header";
import './ResultScreen.css';
import ResultList                 from "./ResultList";
import {connect}                  from "react-redux";
import {numberOfCorrectsSelector} from "../../Quiz/selector";
import {routes}                   from "../../../routes";
import Link                       from "react-router-dom/es/Link";


class ResultScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {numberOfCorrects} = this.props;
        return (
            <div className="result">
                <div className="title">
                    <Header text={`You scored\n${numberOfCorrects}/10`}/>
                </div>
                <ResultList/>
                <Link className="play_again" to={routes.quiz}>
                    PLAY AGAIN?
                </Link>

            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        numberOfCorrects: numberOfCorrectsSelector(state)
    };
}

export default connect(mapStateToProps)(ResultScreen);