import React, {Component} from 'react';
import {connect}          from "react-redux";
import './ResultScreen.css';

import {isCorrectListSelector, quizListSelector} from "../../Quiz/selector";

class ResultList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {quizList, is_correct_list} = this.props;
        return (
            <div className="list">
                {quizList.map((q, i)=>{
                    const sign = is_correct_list[i] ? '+' : '-';
                    const html = sign + q.question;
                    return <div key={q.question} className="item" dangerouslySetInnerHTML={{ __html: html }}/>
                })}
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        quizList: quizListSelector(state),
        is_correct_list: isCorrectListSelector(state)
    };
}

export default connect(mapStateToProps)(ResultList);