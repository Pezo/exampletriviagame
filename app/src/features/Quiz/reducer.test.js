import React                   from 'react';
import {reducer, initialState} from './reducer';
import actions                 from "./actions";

describe('reducer', () => {
    let quizList = ["q1","q2"];
    let loadedState = {quizList, quizIdx: 0, fetched: true, is_correct_list: []};

    it('should return the initial state when no action passed', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should LOAD quizlist', () => {
        expect(reducer(initialState, actions.quizListFetchResolved(quizList))).toEqual(loadedState);
    });

    it('should UPDATE current quiz', () => {
        let nextState = {quizList, quizIdx: 1, fetched: true, is_correct_list: [true]};
        expect(reducer(loadedState, actions.updateQuiz(true))).toEqual(nextState);
    });
});
