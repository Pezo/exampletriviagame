import actions    from "./actions";
import fetchLogic from "./fetchLogic";
import types      from "../../shared/types";
import axios      from 'axios';

import {reducer} from "./reducer";

const createMockStore = require('redux-logic-test').default.createMockStore;

it('fetchLogic should resolve (live endpoint)', () => {
    const store = createMockStore(
        {initialState: {quizList: null, quizIdx: null},
        injectedDeps: {
            httpClient: axios
        },
         logic: [fetchLogic],
        });

    store.dispatch(actions.fetchQuizList());
    return store.whenComplete(() => {
        expect(store.actions[0]).toEqual({type: types.FETCH_QUIZLIST});
        expect(store.actions[1].type).toEqual(types.QUIZLIST_FETCH_RESOLVED);
    });
});

it('fetchLogic resolve + reducer should load data', () => {
    const resp =
              {
                  data: {
                      results: [{
                          category         : 'History',
                          type             : 'boolean',
                          difficulty       : 'hard',
                          question         : 'Japan was part of the Allied Powers during World War I.',
                          correct_answer   : 'True',
                          incorrect_answers: ['False']
                      },
                          {
                              category         : 'History',
                              type             : 'boolean',
                              difficulty       : 'hard',
                              question         : 'The Kingdom of Prussia briefly held land in Estonia.',
                              correct_answer   : 'False',
                              incorrect_answers: ['True']
                          }]
                  }
              };

    const store = createMockStore(
        {initialState: {quizList: null, quizIdx: null, is_correct_list:null},
         reducer: reducer,
         injectedDeps: {
            httpClient: {
                get() { return Promise.resolve(resp); }
            }
         },
         logic: [fetchLogic],
        });

    store.dispatch(actions.fetchQuizList());
    return store.whenComplete(() => {
        expect(store.actions[0]).toEqual({type: types.FETCH_QUIZLIST});
        expect(store.actions[1].type).toEqual(types.QUIZLIST_FETCH_RESOLVED);
        expect(store.actions[1].quizList).toEqual(resp.data.results);
        expect(store.getState()).toEqual({quizIdx: 0, quizList: resp.data.results, fetched:true, is_correct_list:[]});
    });
});