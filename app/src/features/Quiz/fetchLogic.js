import types   from '../../shared/types'
import actions from "./actions";
const createLogic = require('redux-logic').default.createLogic;

const fetchLogic = createLogic({
    type: types.FETCH_QUIZLIST,

    async process({ httpClient }, dispatch, done) {
    try {
      const quizList =
          await httpClient.get("https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean")
                          .then(resp => resp.data.results);
      dispatch(actions.quizListFetchResolved(quizList));
    } catch(err) {
      dispatch(actions.quizListFetchRejected(err));
    }
    done();
  }
});

export default fetchLogic;