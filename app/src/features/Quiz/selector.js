import _ from "lodash";


export function categorySelector(state){
    const {quizList, quizIdx} = state.quiz;
    return _.get(quizList,`[${quizIdx}].category`);
}

export function questionSelector(state){
    const {quizList, quizIdx} = state.quiz;
    return _.get(quizList,`[${quizIdx}].question`);
}

export function levelNumberSelector(state){
    const {quizIdx} = state.quiz;
    return quizIdx + 1;
}

export function correctAnswerSelector(state){
    const {quizList, quizIdx} = state.quiz;
    return _.get(quizList,`[${quizIdx}].correct_answer`);
}

export function fetchedSelector(state){
    const {fetched} = state.quiz;
    return fetched;
}

export function quizListSelector(state) {
    return state.quiz.quizList;
}

export function isCorrectListSelector(state){
    return state.quiz.is_correct_list;
}

export function numberOfCorrectsSelector(state) {
    return _.countBy(state.quiz.is_correct_list)['true'];
}