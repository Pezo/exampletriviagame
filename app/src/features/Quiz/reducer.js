import types from '../../shared/types';
import _ from 'lodash';

export const initialState = {
    quizList: null,
    quizIdx: null,
    fetched: null,
    is_correct_list: [],
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {

        case types.RESET_GAME:
            return {
                ...state,
                fetched: false,
            };

        case types.FETCH_QUIZLIST:
            return{
                ...state,
                quizList: null,
                quizIdx: null,
                fetched: false,
                is_correct_list: [],
            };

        case types.QUIZLIST_FETCH_RESOLVED:
            return {
                ...state,
                quizList: action.quizList,
                quizIdx: 0,
                fetched: true,
                is_correct_list: [],
            };

        case types.UPDATE_QUIZ:
            const new_is_correct_list = _.cloneDeep(state.is_correct_list);
            new_is_correct_list.push(action.is_correct);
            return {
                ...state,
                quizIdx: state.quizIdx + 1,
                is_correct_list: new_is_correct_list
            };

        default:
            return state;
    }
};
