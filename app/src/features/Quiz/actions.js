import types from "../../shared/types";

const actions = {
    fetchQuizList(){
        return {
            type: types.FETCH_QUIZLIST
        }
    },
    quizListFetchResolved(quizList) {
        return {
            type: types.QUIZLIST_FETCH_RESOLVED,
            quizList,
        };
    },
    updateQuiz(is_correct){
        return {
            type      : types.UPDATE_QUIZ,
            is_correct
        }
    },
    quizListFetchRejected(err){
        return {
            type: types.FETCH_QUIZLIST_REJECTED,
            err
        }
    },
    resetGame(){
        return {
            type: types.RESET_GAME
        }
    }

};

export default actions;