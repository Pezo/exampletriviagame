import _                     from 'lodash'
import types                 from "../../shared/types";
import history               from "../../appHistory";
import {routes}              from "../../routes";
import {levelNumberSelector} from "./selector";
import actions               from "./actions";

const createLogic = require('redux-logic').default.createLogic;

const gameEndLogic = createLogic({
    type: types.UPDATE_QUIZ,

    process({ getState, action, ctx }, dispatch, done) {
        if(levelNumberSelector(getState()) > 10){
            dispatch(actions.resetGame());
            history.push(routes.result);
        }
        done();
    }
});

export default gameEndLogic;