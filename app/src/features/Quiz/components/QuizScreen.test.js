import React        from 'react';
import {QuizScreen} from './QuizScreen';
import {shallow}  from 'enzyme';


it('renders <QuizScreen/>', () => {
    let _QuizScreen = shallow(<QuizScreen category={"Category"} question="Question" levelNumber={1} correctAnswer="False" updateQuiz={jest.fn()} fetchQuizList={jest.fn()}/>);
    expect(_QuizScreen).toMatchSnapshot();
});

