import React, {Component} from 'react';
import Header             from "../../../shared/components/Header";
import './QuizScreen.css';
import {connect}          from "react-redux";
import {
    categorySelector,
    correctAnswerSelector, fetchedSelector,
    levelNumberSelector,
    questionSelector
} from "../selector";
import actions            from "../actions";

export class QuizScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchQuizList();
    }

    render() {
        const {category, question, levelNumber, correctAnswer, fetched, updateQuiz} = this.props;
        if(!fetched){
            return (
                <div className="quiz">
                    <div>Downloading</div>
                </div>
            )
        }
        else {
            return (
                <div className="quiz">
                    <div className="title">
                        <Header text={category}/>
                    </div>
                    <div className="box">
                        <span dangerouslySetInnerHTML={{ __html: question }} />
                    </div>
                    <div className="level">
                        {levelNumber} of 10
                    </div>
                    <div className="true" onClick={()=>updateQuiz('True'===correctAnswer)}>
                        TRUE
                    </div>
                    <div className="false" onClick={()=>updateQuiz('False'===correctAnswer)}>
                        FALSE
                    </div>
                </div>
            );
        }
    }
}

function mapStateToProps(state){
    return {
        category: categorySelector(state),
        question: questionSelector(state),
        levelNumber: levelNumberSelector(state),
        correctAnswer: correctAnswerSelector(state),
        fetched: fetchedSelector(state),
    };
}

const mapDispatchToProps = dispatch => ({
    updateQuiz: (is_correct) => dispatch(actions.updateQuiz(is_correct)),
    fetchQuizList: () => dispatch(actions.fetchQuizList())
});

export default connect(mapStateToProps, mapDispatchToProps)(QuizScreen);