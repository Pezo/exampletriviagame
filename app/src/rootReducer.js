import { combineReducers } from 'redux';
import { reducer as quizReducer} from './features/Quiz/reducer';

const rootReducer = combineReducers({
  quiz: quizReducer,
  // ...
});

export default rootReducer;